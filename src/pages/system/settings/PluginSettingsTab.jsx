import React from 'react';
import {Form, Input, Tooltip, Icon, Cascader, Select, Upload, Checkbox, Button, Collapse , Breadcrumb , Tabs , Radio} from 'antd';
import AppHelper from 'utils/AppHelper';
import 'whatwg-fetch'
const FormItem = Form.Item;
const {TextArea} = Input

class PluginSettingsTab extends React.Component {

    constructor(props){
        super(props);
    }

    render() {
        const {getFieldDecorator} = this.props.form;

        const formItemLayout = {
            labelCol: {
                xs: {span: 20},
                sm: {span: 3},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 14},
            },
        };
        const tailFormItemLayout = {
            wrapperCol: {
                xs: {
                    span: 24,
                    offset: 0,
                },
                sm: {
                    span: 14,
                    offset: 6,
                },
            },
        };

        return (
            <div>
                <Collapse defaultActiveKey={['1','2','3']}>
                    <Collapse.Panel header="七牛云存储" key="1">
                        <FormItem {...formItemLayout} label="access key" hasFeedback >
                            {getFieldDecorator('qiniu_access_key', {
                                rules: [{required: true, message: '请填写access key!', whitespace: true}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="secret key" hasFeedback >
                            {getFieldDecorator('qiniu_secret_key', {
                                rules: [{required: true, message: '请填写secret key!', whitespace: true}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="bucket name" hasFeedback >
                            {getFieldDecorator('qiniu_bucket_name', {
                                rules: [{required: true, message: '请填写bucket name!', whitespace: true}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                        <FormItem {...formItemLayout} label="bucket host name" hasFeedback >
                            {getFieldDecorator('qiniu_bucket_host_name', {
                                rules: [{required: true, message: '请填写bucket host name!', whitespace: true}],
                            })(
                                <Input/>
                            )}
                        </FormItem>
                    </Collapse.Panel>
                    <Collapse.Panel header="阿里大鱼" key="2">
                      <p>43434</p>
                    </Collapse.Panel>
                    <Collapse.Panel header="This is panel header 3" key="3">
                      <p>545454</p>
                    </Collapse.Panel>
                </Collapse>
            </div>
        );
    }
}

export default PluginSettingsTab;